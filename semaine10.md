# Tests

## Image

En utilisant les déclarations suivantes (que vous pouvez modifier au besoin):
```c
struct Couleur {    // Une couleur 32 bits
    unsigned int r; // Niveau de rouge entre 0 et 255
    unsigned int g; // Niveau de vert entre 0 et 255
    unsigned int b; // Niveau de bleu entre 0 et 255
    unsigned int a; // Niveau de transparence
                    // a = 0 -> complètement transparent
                    // a = 1 -> complètement opaque
};

typedef struct {             // Une image
    unsigned int largeur;    // La largeur de l'image
    unsigned int hauteur;    // La hauteur de l'image
    struct Couleur **pixels; // Les pixels de l'image
} *Image;
```
proposez une interface et une implémentation qui permet de manipuler une
structure de données de type `Image`, qui offre les services suivants:

```c
/**
 * Crée une image complètement noire et opaque.
 * 
 * @param largeur  La largeur de l'image
 * @param hauteur  La hauteur de l'image
 * @return         Une image de largeur et hauteur donnée
 */
Image image_noire(unsigned int largeur, unsigned int hauteur);

/**
 * Retourne la couleur d'un pixel dans une image.
 * 
 * @param image  L'image
 * @param i      La ligne où se trouve le pixel
 * @param j      La colonne où se trouve le pixel
 * @return       La couleur du pixel (i,j)
 */
struct Couleur image_obtenirPixel(const Image *image,
                                  unsigned int i,
                                  unsigned int j);

/**
 * Modifie la couleur d'un pixel dans une image.
 * 
 * @param image  L'image à modifier
 * @param i      La ligne où se trouve le pixel
 * @param j      La colonne où se trouve le pixel
 * @param r      Le niveau de rouge du pixel
 * @param g      Le niveau de vert du pixel
 * @param b      Le niveau de bleu du pixel
 * @param a      Le niveau de transparence du pixel
 */
void image_modifierPixel(Image *image,
                         unsigned int i,
                         unsigned int j,
                         unsigned int r,
                         unsigned int g,
                         unsigned int b,
                         unsigned int a);

/**
 * Indique si deux images sont égales.
 *
 * Deux images sont égales si leur pixel à l'indice (i,j) sont de même couleur,
 * pour tout couple (i,j) bien défini.
 * 
 * @param image1  La première image
 * @param image2  La deuxième image
 * @return        Vrai si et seulement si les images sont égales
 */
bool image_sontEgales(const Image image1,
                      const Image image2);

/**
 * Applique une réflexion horizontale à l'image.
 * 
 * @param image  L'image à modifier
 */
void image_reflechirHorizontalement(Image image);

/**
 * Applique une rotation de 90 degrés dans le sens horaire à l'image.
 * 
 * @param image  L'image à modifier
 */
void image_rotationHoraire(Image image);

/**
 * Mélange deux images et retourne le résultat.
 *
 * On mélange deux images en faisant la moyenne pondérée des pixels selon les
 * formules
 *     r = poids * r1 + (1 - poids) * r2
 *     g = poids * g1 + (1 - poids) * g2
 *     b = poids * b1 + (1 - poids) * b2
 * 
 * Par exemple, si poids = 1.0, alors on ne conserve que la première image,
 * si poids = 0.0, on ne conserve que la deuxième image et si poids = 0.5,
 * alors on fait la moyenne entre les niveaux de rouge, de vert et de bleu des
 * deux images.
 * 
 * @param image1  La première image
 * @param image2  La deuxième image
 * @param poids   Le poids à accorder à la première image, entre 0.0 et 1.0
 * @return        L'image mélangée
 */
Image image_melangerImages(const Image image1,
                           const Image image2,
                           float poids);
```

N'oubliez pas de tester les fonctions que vous ajoutez au fur et à mesure, en
prenant bien soin de conserver les tests que vous faites (ils seront utilisés
plus tard).

## CUnit

À l'aide de CUnit, proposez un cadre de tests pour les fonctions que vous avez
implémenté à l'exercice précédent.
   
## Intégration continue
   
Configurez un dépôt sur GitLab pour qu'il lance votre suite de tests de façon
automatique. Pour cela, il vous faudra

- ajouter un fichier `Makefile` qui offre la cible `make test`;
- ajouter un fichier `.gitlab-ci.yml` qui configure les tests automatiques.

*Note* : Pour installer CUnit sur GitLab, il vous faudra ajouter la commande
```sh
apt-get update -qq && apt-get install libcunit1 libcunit1-doc libcunit1-dev
```
dans le fichier `.gitlab-ci.yml`.

## Valgrind

Appelez Valgrind sur votre module `image` pour vérifier s'il n'y a pas de fuite
mémoire. Si vous n'avez pas fourni de destructeur, il devrait y en avoir!
Ajoutez un tel destructeur et confirmez avec Valgrind que toutes les fuites
mémoire sont réglées.
