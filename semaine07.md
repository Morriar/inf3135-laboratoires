# Les branches sous Git, suite

L'objectif de cette séance est de continuer une exploration plus avancée du
logiciel Git.

Vous devez avoir complété la séance précédente pour poursuivre.

## "Annuler" un *merge commit*

Supposons que vous ne vouliez pas faire de fusion entre vos deux branches, mais
plutôt rebaser la branche `renverse` sur la version la plus à jour de `master`
(c'est une opération qu'on vous demandera souvent).

Il est alors possible de "retirer" le *merge commit*. Notez qu'il n'est pas
vraiment détruit et existe toujours, mais il n'est pas référencé par une
branche de sorte qu'on ne le verra plus. La commande pour cela, en supposant
que vous êtes sur la branche `master` est:

```bash
git reset --hard <sha commit>
```
où `<sha commit>` est l'identifiant hexadécimal du *commit* parent de l'état du
projet avant la fusion.

**Remarque**: Si vous vous trouvez bien sur la branche `master` avant la
commande `git reset`, il est aussi possible d'entrer la commande
```bash
git reset --hard HEAD^
```
où `HEAD^` est un raccourci vers le *commit* parent de la branche.

Confirmez avec la commande `git gr` que le *commit* a bien été "annulé".

## Rebaser une branche

La prochaine étape consiste à rebaser votre branche `renverse` sur `master`.
Pour cela, passez sur `renverse` avec la commande `git checkout`. Ensuite,
tapez `git rebase master`. Si vous avez eu des conflits lors de la fusion
précédente, vous aurez également des conflits lors du rebasement. Suivez les
étapes pour régler les conflits et visualisez le résultat avec `git gr`.

Vous remarquerez maintenant que la branche `renverse` est basée sur `master`,
ainsi que la branche `origin/renverse` est basée sur un *commit* plus ancien.
Ceci est normal puisque les deux branches sont maintenant désynchronisées. Si
vous entrez
```sh
git push origin renverse
```
vous aurez un message d'erreur à l'effet que les branches sont incompatibles,
ce qui est normal, puisque vous avez réécrit l'historique en rebasant votre
branche. Il est possible de forcer une synchronisation en faisant
```sh
git push origin renverse -f
```
En tapant maintenant `git gr`, vous verrez que les deux branches sont bel et
bien synchronisée.

Notez que cette opération est acceptable dans certains contextes (comme quand
vous travaillez sur une branche privée non partagée), mais qu'elle peut
entraîner certains risques si des collaborateurs ont commencé à développer du
code à partir de l'ancienne version de votre branche. Par conséquent, il faut
être bien conscient de ce que vous faites lorsque vous faites une commande
telle que `git rebase`.

## Rebasement interactif

Lors du rebasement précédent, vous avez réglé un conflit pour obtenir une
version mise à jour, mais il y a probablement des incohérences logiques. Par
exemple, votre fonction s'appelle `arrayReverse` alors qu'elle devrait se
nommer `Array_reverse` pour respecter la logique introduite par le *commit* le
plus récent de la branche `master`.

Pour rectifier la situation, il peut être approprié dans certains cas de faire
un rebasement interactif. Placez-vous sur la branche `renverse` avec
`git checkout` et tapez
```sh
git rebase -i master
```
Un dialogue s'ouvrira (normalement, c'est Vim qui est lancé) dans lequel on
vous demande d'indiquer comment faire le rebasement. Il y a même un aide
mémoire en commentaire qui est indiqué. Changez le mot `pick` pour le mot
`edit` pour chacun des *commits* que vous souhaitez modifier pour régler
l'incohérence mentionnée plus haut, puis sauvegardez et quitter.

À partir de ce moment, vous pourrez modifier chacun des *commits* que vous avez
indiqué comme d'habitude. Pour cela, il suffit de faire les modifications
directement dans le fichier et d'entrer `git commit -a --amend` une fois que
vous avez terminé. Puis vous entrez `git rebase --continue` pour passer au
prochain *commit* à modifier.

Cette opération peut paraître inquiétante la première fois qu'on la fait. Si ça
peut vous rassurer, sachez que que le rebasement **duplique** les **commits**,
de sorte que si vous manquez votre coup, vous pouvez toujours recommencer, soit
avec la commande `git rebase --abort`, soit en vous plaçant sur la branche
`origin/renverse` avec `git checkout`, puis en remettant votre branche
`renverse` là où elle se trouvait avant avec la commande

```sh
git checkout -B renverse
```
(Le `B` doit être majuscule pour forcer le déplacement de la branche).

## Faire une requête d'intégration

Une fois que votre branche `renverse` a été rebasée sur `master` (elle est donc
à jour avec le programme) et qu'elle a bien été nettoyée avec `git rebase -i`,
rendez-vous sur l'interface de GitLab et créez une requête d'intégration.
Faites la requête depuis votre branche `renverse` vers la branche `master` de
votre propre dépôt (comme vous aurez à le faire dans le travail pratique 2).
Prenez la peine d'écrire un titre significatif et ensuite de décrire les
modifications brièvement avec le format Markdown.

## Arbitrer une requête d'intégration

Donnez un accès en mode *Developer* à un de vos coéquipiers pour qu'il récupère
votre branche et qu'il l'examine, qu'il commente vos modifications, etc. Notez
que la procédure pour récupérer la branche d'un coéquipier est la même que
lorsqu'on gère des dépôts *origin* et *upstream*: il suffit d'ajouter le dépôt
de votre coéquipier avec
```sh
git remote add <nom dépôt> <url dépôt>
```
(personnellement, je choisis le nom d'utilisateur GitLab de la personne pour
`<nom dépôt>`, mais vous pouvez mettre n'importe quoi), puis ensuite d'entrer la
commande `git fetch <nom dépôt>` pour récupérer les branches de votre
coéquipier, qui apparaîtront avec la commande `git gr`. Ensuite, pour vérifier
l'état de la branche, il suffit d'utiliser la commande `git checkout <nom
dépôt>/<nom branche>` et de jouer avec le code de votre coéquipier.
