# Séance 1: Unix, Vim, Markdown et Git

## 1 - Installation d'un système Unix

(Note: cet exercice n'est pas abordé en laboratoire et devrait être complété à
la maison.) Si vous avez un ordinateur portable et que vous souhaitez
l'utiliser pour développer vos travaux pratiques, il est important d'installer
le plus rapidement possible un système Unix sur votre machine. Voici les
environnements recommandés dans le cadre de ce cours :

- Utilisateurs *Linux*. Vous n'avez rien à faire et vous êtes probablement déjà
  fonctionnels sur un système Unix.

- Utilisateurs *Mac OS*. Assurez-vous d'avoir installé XCode. Je vous
  recommande aussi d'installer un gestionnaire de paquets, qui simplifiera
  l'installation de programmes tout au long du cours. J'utilise personnellement
  [MacPorts](https://www.macports.org/), mais j'ai aussi entendu dire que
  [Homebrew](https://brew.sh/index_fr.html) fonctionnait bien (notez qu'on ne
  peut installer qu'un seul des deux gestionnaires). À partir de là, vous
  pourrez facilement installer Git, Vim, gcc, etc.

- Utilisateurs *Windows*. Il est fortement recommandé d'installer un système
  Linux sur votre machine personnelle. En particulier, vous ne pourrez pas
  compléter le TP3 sur le serveur Java, qui n'offre pas d'interface graphique.
  Il y a donc deux possibilités qui s'offrent à vous:

  - (**recommandé**) Installer un système Linux en partition double (*dual
    boot*). Cette solution est préférable à la suivante si vous souhaitez avoir
    une meilleure expérience et moins de problèmes techniques à résoudre
    (délais, utilisation sous-optimale du matériel, etc.). S'il s'agit de votre
    première expérience Linux, je vous recommande d'utiliser
    [Ubuntu](https://www.ubuntu.com/) ou [Linux
    Mint](https://www.linuxmint.com/)
  - Installer une machine virtuelle Linux. Voir par exemple la capsule
    <https://www.youtube.com/watch?v=1zfO-Fhqyb8> du CLibre sur le sujet.

  Le CLibre est une organisation qui relève de la Faculté des sciences et donc
  le mandat est de promouvoir le logiciel libre. N'hésitez pas à les contacter
  si vous avez besoin d'aide pour cette étape (<http://clibre.uqam.ca/>).

## 2 - Établir une connexion SSH

Dans le cadre de ce cours, des comptes ont été créés pour chacun d'entre vous
sur le serveur Java (`java.labunix.uqam.ca`). Pour vous y connecter, vous devez
connaître votre identifiant MS (2 lettres suivies de 6 chiffres) ainsi que
votre NIP.

Familiarisez-vous avec les étapes de base pour établir une connexion SSH. Le
mécanisme sera différent selon que vous travaillez sous Windows, Mac OS ou
Linux. Il s'agit d'une partie très importante, car vos deux premiers travaux
pratiques devront minimalement fonctionner sur le serveur Java.

Parfois, certains comptes étudiants ne sont pas activés sur les serveurs.
Si c'est le cas, rendez-vous au comptoir d'accueil des laboratoires.

## 3 - Vim

Complétez le tutoriel intégré de Vim. Pour cela, il suffit d'ouvrir un terminal
et de taper la commande

```shell
vimtutor
```

Puis suivez les instructions.

Ensuite, téléchargez le fichier `.vimrc` disponible dans le répertoire
`exemples` et placez-le dans le répertoire home. Il faut également installer le
greffon [Vim-plug](https://github.com/junegunn/vim-plug). Ces étapes sont un
peu longues, mais elle vous donneront une configuration minimale pour
travailler efficacement sous Vim pendant tout le cours.

## 4 - Compilation d'un programme C

- Créez un programme C nommé `hello.c` qui affiche le traditionnel "Hello,
  world!" et sauvegardez-le. Vous pouvez simplement copier les lignes suivantes:

    ```c
    #include <stdio.h>

    int main(int argc, char *argv[]) {
        printf("Hello, world!\n");
        return 0;
    }
    ```

- Compilez le fichier en une seule étape

    ```shell
    gcc hello.c
    ```

et exécutez le fichier résultant en entrant `./a.out` pour vous assurer que
tout fonctionne.

Il est aussi possible que cet exécutable ait un nom que vous choisissez, par
exemple:

```shell
gcc hello.c -o hello
```

## 5 - Makefile

Finalement, créez un fichier `Makefile` qui automatise l'étape de compilation.

Ajoutez à ce fichier Makefile une cible `clean` qui supprime le fichier `.o` ainsi
que l'exécutable.

## 6 - Markdown

Dans le même répertoire que le fichier `hello.c` que vous avez conçu dans les
exercices précédents, créez un fichier nommé `README.md` qui décrit brièvement
votre projet. Indiquez minimalement le titre du projet, l'auteur du projet et
une ou deux phrases qui décrivent ce qu'il fait, ainsi qu'une section qui
décrit tous les fichiers présents dans le répertoire. Profitez-en pour explorer
la syntaxe Markdown:

- Titres et sections;
- Paragraphes;
- Bouts de code;
- Italique et gras;
- Insertion d'une image;
- etc.

## 7 - Création d'un projet avec Git

- Rendez-vous tout d'abord sur le site de [GitLab](https://gitlab.com/) pour
  vous créer un compte.

- Créez un nouveau dépôt, donnez-lui un nom significatif, laissez la
  description vide et les autres options par défaut puis confirmez la création.

- Maintenant, à l'aide de Vim, ouvrez le fichier `~/.gitconfig` et entrez les
  lignes suivantes

    ```ini
    [user]
        name = <votre nom>
        email = <votre courriel>
    [color]
        ui = auto
    [core]
        editor = vim
    ```

    (Lorsque vous deviendrez plus à l'aise avec Git, il est possible que vous
    ayez à modifier ce fichier à nouveau, mais ça devrait vous suffire pour la
    première partie du cours.)

    Notez que quand vous créez un nouveau dépôt dans GitLab ou GitHub, il vous
    est demandé de taper les commandes suivantes:

	```
	git config --global user.name "Nom Prénom"
	git config --global user.email "email@domaine.ext"
	```

    La commande `git config --global <clé> <valeur>` sert à modifier le fichier
    `~/.gitconfig`. Lors de la cŕeation de vos prochains dépôts, vous n'aurez
    pas besoin de taper ces commandes car votre configuration est déjà définie.

- Dans un terminal, rendez-vous dans le répertoire qui contient votre projet
  avec les fichiers `hello.c` et `README.md`. Entrez la commande

    ```shell
    git init
    ```

    qui initialise le projet.

- Ensuite, indiquez à Git que vous souhaitez versionner les deux fichiers
  décrits plus haut

    ```shell
    git add hello.c README.md
    ```

- Si vous tapez `git status`, vous devriez voir que les deux fichiers sont
  ajoutés, mais que d'autres fichiers (comme `a.out`) n'ont pas été ajoutés.
  **Ne les ajoutez pas**, car ils ne doivent pas être versionnés par Git.

- Toujours dans le répertoire courant, créez un fichier nommé `.gitignore` (le
  point initial est important) dans lequel vous insérez ce qui suit :

    ```shell
    a.out
    ```

- Ajoutez ce fichier également à l'aide de la commande `git add .gitignore`.

- Ensuite, entrez

    ```shell
    git commit
    ```

- Cela devrait ouvrir Vim pour que vous écriviez un message de commit. Écrivez
  quelque chose du genre

    ```shell
    Première version de mon programme Hello, world!
    ```

- Ensuite, il vous faut établir le lien entre votre dépôt local et celui sur
  GitLab. Pour cela, il suffit d'entrer la commande

    ```shell
    git remote add origin git@gitlab.com:<nom d'utilisateur>/<nom du projet>.git
    ```

- Finalement, entrez la commande

    ```shell
    git push origin master
    ```

- Ceci devrait avoir pour effet de "pousser" vos modifications locales sur le
  dépôt à distance. Vous pouvez le visualiser dans un fureteur. En particulier,
  votre fichier `README.md` devrait apparaître sous forme HTML dans le bas de
  la page d'accueil du projet. Assurez-vous toujours de bien respecter le
  format Markdown pour que la génération du fichier HTML apparaisse
  correctement en visualisant l'ensemble du fichier.
