# Généricité et types de programmation

## Tri rapide

Écrivez un petit programme C qui fait appel à la fonction ``qsort`` de la
bibliothèque ``stdlib.h`` et qui trie en ordre alphabétique (lexicographique)
les capitales que vous avez obtenues à la première question de la séance
précédente.

## Fouille binaire

Étendez l'exemple précédent pour qu'il soit possible d'effectuer une recherche
binaire à l'aide de la fonction ``bsearch`` de la bibliothèque ``stdlib.h``.

## Programmation par contrat/programmation défensive

Reprenez le module ``treemap`` étudié en classe et en démonstration et
assurez-vous qu'il soit le plus robuste possible, en lui ajoutant des
assertions ou en utilisant des stratégies de programmation défensive.
