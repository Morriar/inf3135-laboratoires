# Les branches sous Git

L'objectif de cette séance est de se familiariser avec les branches sous Git.

## Configuration

Dans un premier temps, assurez-vous d'avoir bien configuré votre environnement
de travail. En particulier, il est important de mettre à jour votre fichier
`.gitconfig` en vous inspirant de celui qui se trouve dans le répertoire
`exemples` de ce dépôt. Un synonyme (*alias*) particulièrement utile est

```bash
graph = log --graph --full-history --all --color --pretty=tformat:"%x1b[31m%h%x09%x1b[32m%d%x1b[0m%x20%s%x20%x1b[33m(%an)%x1b[0m"
```

qui permet d'afficher le graphe de l'historique de votre projet en ligne de
commande. Dans un deuxième temps, je vous recommande de modifier votre invite
de commande pour qu'elle affiche toujours la branche sur laquelle vous vous
trouvez. Vous pouvez vous inspirer du fichier `exemples/.profile` de ce dépôt
pour cela. Les lignes que pertinentes sont

```bash
function parse_git_branch_and_add_brackets {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\ \[\1\]/'
  }
export LSCOLORS=CxdxxxxxExxxxxExExCxCx
PS1="\n\e[36m\$(parse_git_branch_and_add_brackets) \e[32;1m\u\e[0m \e[33;1m[\w]\e[0m\n $ "
```

Si vous êtes sur Linux, vous devez ajouter ces lignes dans votre fichier `.bashrc`.

## Explorer un dépôt

- Rendez-vous sur https://gitlab.com/ablondin/array et dupliquez
  (*fork*) le dépôt. Notez qu'il est public, mais vous devez en faire une copie
  privée (comme pour le TP2).
- Ensuite, clonez le dépôt sur votre espace de travail personnel (machine ou
  sur serveur Malt/Java).
- Explorez l'historique du dépôt :

  - `git log`: pour voir l'historique linéaire des *commits*;
  - `git graph`: pour visualiser le graphe avec les différentes branches. En
    particulier, repérez les identifiants uniques des *commits*, les branches,
    l'état actuel du projet (`HEAD`), etc.
  - Étudiez ce qui a été fait dans chacun des *commits*. Il n'y en a pas
    beaucoup, donc ça vaut la peine de regarder. Vous pouvez voir les
    modifications apportées par un *commit* avec la commande `git show`.
  - Naviguez entre les branches `master` et `origin/renverse` avec la commande
    `git checkout`. Comparez la différence entre les versions avec la commande
    `git diff`.

- Remarquez que la branche `origin/renverse` est en retard par rapport à la
  branche `master`. C'est normal, l'objectif de cette séance étant de vous
  illustrer comment vous pouvez synchroniser différentes versions d'un même
  projet.

## Complétion de la branche `renverse`

Rendez-vous sur la branche `origin/renverse` avec la commande
```sh
git checkout origin/renverse
```
Notez que vous êtes en mode "détaché", car la branche `origin/renverse` est une
branche distante. Vous allez donc créer une branche `renverse` locale sur
laquelle vous allez travailler:
```sh
git checkout -b renverse
```
Maintenant, en tapant `git graph`, vous verrez que votre branche locale et la
branche distante pointent au même endroit.

**Remarque**: On peut remplacer les deux commandes
```sh
git checkout origin/renverse
git checkout -b renverse
```
par
```sh
git checkout -b renverse --track origin/renverse
```

La notion de branche **locale** et de branche **distante** (en anglais,
*remote*) est fondamentale sous Git. Il est important de distinguer ces deux
types de branches, car lorsqu'on se synchronise avec un dépôt distant, il
s'agit essentiellement de synchroniser les branches locales et les branches
distantes.

Complétez maintenant l'implémentation de la fonction `arrayReverse` qui
renverse les valeurs d'un tableau (notez que le tableau est modifié, vous ne
retournez pas une copie). Sauvegardez vos modifications avec `git commit`.
Prenez le temps d'écrire un message de *commit* **pertinent**, en respectant la
syntaxe (première lettre majuscule, un point à la fin).

Ajoutez un ou deux tests dans la fonction `main` qui illustre que vous avez
bien implémenté la fonction `arrayReverse` (cette étape est importante pour la
suite).

Finalement, poussez votre branche `renverse` vers `origin` (votre *fork*):
```sh
git push origin renverse
```
Vous devriez maintenant voir que la branche `renverse` a été mise à jour sur
l'interface GitLab avec vos modifications. Familiarisez-vous avec l'interface
web qui vous permet de visualiser les modifications apportées par cette
branche.

## Fusionner deux branches

Lorsque vous avez complété la branche `renverse`, rendez-vous sur la branche
`master` avec `git checkout`. Ensuite, fusionnez `master` avec `renverse` à
l'aide de la commande
```sh
git merge renverse
```
Il est probable que vous ayez un conflit, à tout le moins dans la fonction
`main`, si vous avez pris la peine d'ajouter des tests. Réglez-les conflits et
faites `git commit`. Vous aurez alors un *commit* spécial appelé *merge
commit*. Il s'agit de *commits* qui ont deux "parents". C'est souvent au moment
d'une fusion qu'un conflit survient, mais parfois, la fusion se fait
automatiquement sans conflit.
