# Les modules en C

## Programmation modulaire

L'objectif de cette question est de construire une petite application
décomposée en modules :

* En utilisant des programmes Unix (moi, je l'ai fait avec seulement `grep` et
  `sed`), récupérez le nom de toutes les capitales stockées dans le fichier
  `countries.json` disponible dans le projet Github
  [countries](https://github.com/mledoze/countries/) et stockez le résultat
  dans un fichier texte nommé `capitales.txt`. Au besoin, n'hésitez pas à
  modifier le fichier manuellement, par exemple pour éliminer certains noms
  avec des caractères spéciaux ou des lignes vides, l'objectif n'étant pas
  d'avoir un résultat parfait, mais un résultat approximatif. Ajoutez également
  le code de trois lettres du pays correspondant à la capitale (dans le fichier
  `countries.json`, ces codes sont identifiés par la chaîne `cca3`).
  Sauvegardez le résultat dans un fichier texte `countries.txt`, pour que les
  premières lignes ressemblent à
  ```
  ABW,Oranjestad
  AFG,Kabul
  AGO,Luanda
  AIA,The Valley
  ...
  ```

* Reprenez l'exemple de la structure de données `Treemap` présentée en classe
  et disponible dans le répertoire `exemples` du dépôt des exercices. et
  transformez-le en module avec une interface `treemap.h` et une implémentation
  séparée `treemap.c`.
* Compilez manuellement le module `treemap`.
* Concevez un autre module (sans interface) d'un seul fichier `country.c` qui
  contient une fonction `main` et qui stocke les données calculées plus haut
  dans une instance de `Treemap` (les clés sont les codes de 3 lettres et les
  valeurs sont les capitales).
* Incluez un Makefile manuel permettant de compiler automatiquement les 2
  modules.
* Rendez le Makefile précédent plus générique.

## La bibliothèque Jansson

Refaites l'exercice précédent en utilisant la bibliothèque
[Jansson](http://www.digip.org/jansson/). Il s'agit d'un bon exercice de
refactorisation de votre code. Plus précisément:

1. `country.c` utilise actuellement la liste produite par grep.
2. Assurez-vous que tout fonctionne en sauvegardant quelques sorties console
   dans des fichiers.
3. Modifiez `country.c` pour lire depuis un fichiers JSON avec Jansson.
4. Vérifiez que rien n'est brisé en faisant des `diff`s avec les fichiers
   produits à l'étape 2.

## La bibliothèque Cairo

En utilisant la bibliothèque Cairo, écrivez un petit programme qui génère une
grille m x n. Par exemple, si vous entrez la commande

```sh
./grille 5 4 20
```

alors vous devriez obtenir un fichier PNG contenant une grille de 5 lignes et 4
colonnes, avec un espacement de 20 unités (bref, chaque carré est de dimension
20 par 20 pixels.
