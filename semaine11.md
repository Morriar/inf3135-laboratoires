# Scripts

## Script de démarrage de projet

Écrire un script permettant d'initialiser un nouveau répertoire pour un projet C.

Par exemple, la commande:

~~~sh
./creer_projet ./mon_projet
~~~

Permet de:

1. créer le répertoire `mon_projet` s'il n'existe pas, ou affiche un message indiquant que le répertoire existe déjà et quitte
2. ajoute les fichiers de base (`.gitignore`, `Makefile`, `README`, `src/main.c`)
3. initialise le dépôt git
4. commit les fichiers ajoutés

Pensez à vérifier la présence de l'argument et à afficher de l'aide en cas d'erreur.
Le nouveau projet doit pouvoir se compiler et s'exécuter directement avec la commande `make`.
Lorsque le nouveau projet s'exécute, il affiche `Hello, nouveau projet`.

Vous pouvez soit écrire les fichiers directement depuis shell, soit copier les fichiers
depuis un répertoire contenant le gabarit du projet.

Afin que votre script soit exécutable depuis n'importe où sur la machine, vous pouvez ajouter le répertoire du script à votre variable `PATH`.

Commandes utiles:

* `mkdir`
* `echo`
* [here-docs](http://tldp.org/LDP/abs/html/here-docs.html)
* `cp`
* `git`

## `getops`

Utilisez la commande `getopts` pour ajouter un paramètre optionnel à la commande:

~~~sh
./creer_projet -t ./mon_exemple ./mon_projet
~~~

Permet d'initialiser le répertoire `mon_projet` avec le contenu du répertoire
`mon_exemple`.

Dans ce cas, le script doit:

1. créer le répertoire `mon_projet`
2. copier le contenu du répertoire d'exemple dans le répertoire `mon_projet` (y compris les sous-dossiers)
3. initialiser le dépôt git
4. commiter tous les fichiers

Commandes utiles:

* `getopts`
* `cp`
* `git`

## Script de test

Écrivez un nouveau script permettant de tester le premier.

Voici quelques comportements que l'on souhaite tester:

* l'appel de `creer_projet` sans argument provoque l'affichage de l'aide (*usage*)
* l'appel de `creer_projet` sans l'option `-t` crée le répertoire avec les fichiers par défaut et initialise le dépôt git
* l'appel de `creer_projet` avec l'option `-t` crée le répertoire avec les fichiers issus de l'exemple et initialise le dépôt git

Vous pouvez vous inspirer du script de test pour le TP1.
Pensez à nettoyer les fichiers après les tests.

Commandes utiles:

* `creer_projet`
* `test`
* `ls`
* `diff`

## Script de sauvegarde

Écrivez un script permettant de sauvegarder automatiquement votre travail sur un autre serveur.

Par exemple, de votre machine personelle vers `java` (ou de `java` vers `malt` pour la démonstration).

Ainsi, la commande:

~~~sh
./sauver_projet ./mon_projet
~~~

Permet de:

1. Compresser le répertoire `mon_projet` en `tar.gz`
2. Copier l'archive vers votre répertoire home dans un répertoire appelé `.sauvegardes`
3. Pour chaque répertoire sauvegardé avec la commande, on ne veut garder que les 5 dernières
   sauvegardes.
   Vous devez donc vérifier quelles sont les sauvegardes existantes pour le répertoire
   dans `.sauvegardes` et supprimer les plus anciennes lorsque nécessaire.

Commandes utiles:

* `date`
* `tar`
* `scp`
* `ssh`

Si vous éprouvez des difficultés avec `ssh`, vous pouvez faire l'exercice en créant les sauvegardes
dans un autre répertoire sur la même machine.

## Automatiser la sauvegarde

Utilisez `cron` pour automatiser l'appel à `./sauver_projet ./mon_projet` toutes les heures.

Commandes utiles:

* `crontab`
